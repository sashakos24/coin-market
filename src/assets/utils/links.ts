export const COINCAP_API = `https://api.coincap.io`;
export const COINS_API = `${COINCAP_API}/v2/assets`;
export const IMAGES_API = `https://s2.coinmarketcap.com/static/img/coins/64x64`;
