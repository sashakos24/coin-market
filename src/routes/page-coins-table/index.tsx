import React, { ChangeEvent, useEffect, useState } from 'react';
import styles from './styles.module.scss';
import InputSearch from '../../components/InputSearch';
import Layout from '../../components/layout/Layout';
import FixButton from '../../components/FixButton';
import FixButtonBottomRight from '../../components/FixButtonBottomRight';
import TableCoins from '../../components/table-coins';
import { useLazyGetCoinsQuery } from '../../redux/api/coins';
import { useAppDispatch } from '../../redux/hooks';
import { setAllCoins, setCoinsForTable } from '../../redux/slice/coins';
import TextWriper from '../../components/TextWriper';

const PageCoinsTable = () => {
	const offsetQuery = 0;
	const [limitQuery, setLimitQuery] = useState(20);
	const [getCoinsQuery, { isLoading, error }] = useLazyGetCoinsQuery();
	const dispatch = useAppDispatch();

	useEffect(() => {
		getDataQuery(limitQuery, offsetQuery).then();
	}, [offsetQuery, limitQuery]);

	const getDataQuery = async (limit: number, offset: number) => {
		const responseTable = await getCoinsQuery({ limit, offset })
			.unwrap();
		const responseAll = await getCoinsQuery({ limit: 2000, offset: 0 })
			.unwrap();
		dispatch(setAllCoins(responseAll));
		dispatch(setCoinsForTable(responseTable));
	};

	const handleLoadMoreCoins = () => {
		setLimitQuery(prevState => prevState + 20);
	};

	const handleScrollTop = () => {
		window.scrollTo({
			top: 0,
			behavior: 'smooth',
		});
	};

	const handleScrollDown = () => {
		window.scrollTo({
			top: document.documentElement.scrollHeight,
			behavior: 'smooth',
		});
	};

	const [inputSearchValue, setInputSearchValue] = useState('');

	return (
		<div className={styles.page}>
			<Layout title={'Table page'}>
				<div className={styles.page_content}>
					<InputSearch
						inputText={'Search by name'}
						onChange={(event: ChangeEvent<HTMLInputElement>) => {
							setInputSearchValue(event.target.value);
						}}
					/>
					<div className={styles.btn_scroll_down}>
						<FixButton text={'To down'} onClick={handleScrollDown} />
					</div>
					{isLoading ? <TextWriper text={'Loading...'} delay={300} />
						: error ? <div>Error</div>
							:
							<TableCoins handleMoreCoins={handleLoadMoreCoins} inputSearch={inputSearchValue} />
					}
					<FixButtonBottomRight text={'To top'} onClick={handleScrollTop} />
				</div>
			</Layout>
		</div>
	);
};

export default PageCoinsTable;
