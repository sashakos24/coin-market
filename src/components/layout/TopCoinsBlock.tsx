import React, { useEffect } from 'react';
import styles from './styles.module.scss';
import { ICoin } from 'models/ICoin';
import TopCoinHeader from './TopCoinHeader';
import { useAppSelector } from '../../redux/hooks';

const TopCoinsBlock = () => {
	const countCoins = 3;
	const coinsAll = useAppSelector(state => state.coinsAll.coinsAll);
	useEffect(() => {
		console.log(coinsAll);
	}, []);

	return (
		<div className={styles.top_coins_block}>
			<div className={styles.top_coins_title}>
				{`Top ${countCoins} coins: `}
			</div>
			<div className={styles.top_coins}>
				{coinsAll?.slice(0, countCoins)?.map((coin: ICoin, key: number) =>
					<TopCoinHeader coin={coin} key={key} />,
				)
				}
			</div>
		</div>
	);
};

export default TopCoinsBlock;
